import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductoComponent } from './producto/producto.component';
import { LoginComponent } from './login/login.component';
import {Service} from './services/service';
import { ProductoItemComponent } from './producto-item/producto-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule} from "@angular/common/http";

import { MatButtonModule, MatFormFieldModule, MatInputModule, MatRippleModule } from '@angular/material';




const appRoutes: Routes = [
  { path: 'productos', component: ProductoComponent },
  {
    path: 'login',
    component: LoginComponent,
  },
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ProductoComponent,
    LoginComponent,
    ProductoItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule, ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
  ],
  providers: [Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
