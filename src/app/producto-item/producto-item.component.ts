import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: '[app-producto-item]',
  templateUrl: './producto-item.component.html',
  styleUrls: ['./producto-item.component.scss']
})
export class ProductoItemComponent implements OnInit {

  @Input("producto")
  producto : producto;

  constructor() { }

  ngOnInit() {
  }

}
