import { Component, OnInit } from '@angular/core';
import { Service } from '../services/service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss']
})
export class ProductoComponent implements OnInit {

  public productos: producto[];

  constructor(private service: Service, private router: Router) { 


  }


  ngOnInit() {
    this.service.getProductos()
      .subscribe(r=>{
        this.productos= r.body
      },()=>{
        this.router.navigateByUrl('/login');
      });
  }

  closeSession(){
    sessionStorage.removeItem('key');
    this.router.navigateByUrl('/login');
  }

}
