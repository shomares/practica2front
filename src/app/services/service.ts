import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class Service {

    private url : string;
    constructor(private http: HttpClient){
        this.url = window.location.href.replace('/login','/api')
                                        .replace('/producto','/api');
    }

    getProductos (): Observable<any>
    {
        const key = sessionStorage.getItem('key');
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
              'Authorization': 'bearer ' + key
            }),
            observe: 'response' as 'body'
          };

        return this.http.get<producto[]>(this.url+ "/producto", httpOptions);
    }

    login(user: user): Observable<user>
    {
        return this.http.post<user>(this.url+ "/usuarios", user);
    }

}