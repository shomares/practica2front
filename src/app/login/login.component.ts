import { Component, OnInit } from '@angular/core';
import { Service } from '../services/service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public error: string;

  public loginForm: FormGroup;

  constructor(private service: Service, private router: Router, private formBuilder: FormBuilder) {
    this.loginForm = this.formBuilder.group({
      id: ['', Validators.compose([Validators.required, Validators.email])],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(6)]),
      ],
    });
   }

  ngOnInit() {
  }

  onSubmit(user){
    this.service.login(user)
          .subscribe(r=>{
            sessionStorage.setItem('key', r.token);
            this.router.navigateByUrl('/productos');
          }, ()=>{
            this.error = "Contraseña invalida"
          });
  }

}
